#Gaby Mizrahi, Ariel Goldschlag
#600.120
#Homework 3
#2/27/15
#(516) 368-4159
#gmizrah1, agoldsc2
#gmizrah1@jhu.edu, agoldsc2@jhu.edu

CC = gcc 
GCOV= -fprofile-arcs -ftest-coverage
#CFLAGS = -std=c99 -Wall -Wextra -pedantic         # for final build
CFLAGS = -std=c99 -Wall -Wextra -pedantic -g -O0 $(GCOV) # for debugging and gcov
bin: hw3

test: test_wordsearch
	./test_wordsearch

wordsearch.o: wordsearch.c wordsearch.h
	$(CC) $(CFLAGS) -c wordsearch.c

test_wordsearch.o: test_wordsearch.c wordsearch.h
	$(CC) $(CFLAGS) -c test_wordsearch.c
testgcov: test
	gcov *.c
	cat *.c.gcov

hw3.o: hw3.c wordsearch.h
	$(CC) $(CFLAGS)  -c hw3.c

test_wordsearch: test_wordsearch.o wordsearch.o
	$(CC) $(CFLAGS) -o test_wordsearch test_wordsearch.o wordsearch.o

hw3: hw3.o wordsearch.o
	$(CC) $(CFLAGS) -o hw3 hw3.o wordsearch.o

clean:
	rm -f *.o test_wordsearch hw3 *.c.gcov *.gcno *.gcda
