/*
Gaby Mizrahi, Ariel Goldschlag
600.120
Homework 3
2/27/15
(516) 368-4159
gmizrah1, agoldsc2
gmizrah1@jhu.edu, agoldsc2@jhu.edu
 */
#include "wordsearch.h"
void search_word_right(char** grid, char** wordlist, int wordnum, char* word, int row,int col, int numcols);

//taken from last weeks starter code
bool fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    if (!lhs || !rhs) return false;

    bool match = true;
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = false;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}//end fileeq

void test_check_grid(void)
{
  FILE* infile=fopen("testgrid.txt","w");
  fprintf(infile, "abc\ndef\nghi\n");
  fclose(infile);
  char* fileName="testgrid.txt";
  int rows;
  int cols;
  assert(check_grid(fileName,&rows,&cols));
  assert(rows==3 && cols==3);
  
  infile=fopen("testgrid2.txt","w");
  fprintf(infile, "abcd\ndef\nghi\n");
  fclose(infile);
  fileName="testgrid2.txt";
  assert(!check_grid(fileName,&rows,&cols));

  infile=fopen("testgrid3.txt","w");
  fprintf(infile, "abc\ndefd\nghi\n");
  fclose(infile);
  fileName="testgrid3.txt";
  assert(!check_grid(fileName,&rows,&cols));

  infile=fopen("testgrid4.txt","w");
  fprintf(infile, "abc\ndef\nghih\n");
  fclose(infile);
  fileName="testgrid4.txt";
 assert(!check_grid(fileName,&rows,&cols));

  infile=fopen("testgrid5.txt","w");
  fprintf(infile, "abc\ndef\nghi\n\nslkjflisoe");
  fclose(infile);
  fileName="testgrid5.txt";
  assert(check_grid(fileName,&rows,&cols));
  assert(rows==3 && cols==3);

  infile=fopen("testgrid6.txt","w");
  fprintf(infile, "ABC\nDEF\nGHI\n");
  fclose(infile);
  fileName="testgrid6.txt";
  assert(check_grid(fileName,&rows,&cols));
  assert(rows==3 && cols==3);
}

void test_load_grid(void)
{
  char* fileName="testgrid.txt";
  int rows;
  int cols;
  char** grid;
  check_grid(fileName, &rows, &cols);
  printf("the things i care about ROW: %d, COL:%d\n", rows, cols);
  grid = load_grid(fileName,rows,cols);
  FILE* outfile=fopen("gridoutput.txt","w");
  for(int curRow=0;curRow<rows;curRow++)
  {       
    for(int curCol=0;curCol<cols;curCol++)
    {
      fprintf(outfile,"%c",grid[curRow][curCol]);
    }
    fprintf(outfile,"\n");
  }
  fclose(outfile);
  free(grid);
  assert(fileeq("gridoutput.txt","testgrid.txt"));
}

char* testable_read_word(FILE* infile)
{
  char* wordbuffer = malloc(sizeof(char)*5);
  int spot = 0;
  int length = 5;
  char temp = 'a';
  while (fscanf(infile,"%c", &temp)!= EOF && !isspace((int)temp))
  {
    if (spot == length -1)
    {
      wordbuffer = realloc(wordbuffer, 2*length);
      length *=2;
    }
    wordbuffer[spot] =tolower( temp); 
    spot++;
  }
  wordbuffer = realloc(wordbuffer,spot+1);
  wordbuffer[spot] = '\0';
  return wordbuffer;
}

void test_read_word(void)
{
  FILE* infile = fopen("testword.txt","w");
  fprintf(infile, "THisisalonGWorD ");//test when doubles wordbuffer and deals with uppercase
  fclose(infile);
  infile = fopen("testword.txt", "r");
  char* word;
  word = testable_read_word(infile);
  assert(!strcmp(word, "thisisalongword"));
  assert(strlen(word) == 15); 
  free(word);
}
void test_add_to_word_list(void)
{
  char** wordlist=malloc(sizeof(char*)*5);
  char* word2="happy";
  char* word3="abc";
  char* word1="thisisalongword";
  char* word4="zzzz";
  char* word5="hillel";
  int wordnum=0;
  int listlength=5;
  add_to_word_list(wordlist,word4,&wordnum,&listlength);
  add_to_word_list(wordlist,word1,&wordnum,&listlength);
  add_to_word_list(wordlist,word3,&wordnum,&listlength);
  add_to_word_list(wordlist,word2,&wordnum,&listlength);
  add_to_word_list(wordlist,word5,&wordnum,&listlength);
  add_to_word_list(wordlist,word2,&wordnum,&listlength);
  add_to_word_list(wordlist,word4,&wordnum,&listlength);
  assert(wordnum==7);
  assert (listlength==10);
  qsort(wordlist,wordnum,sizeof(char*),compare_string);
  for(int i=0;i<wordnum;i++)
  {
    printf("%s\n",wordlist[i]);
  }
  free(wordlist);
}

void test_binary_search(void)
{
  char** wordlist=malloc(5*sizeof(char*));
  wordlist[0] = "help";
  wordlist[1] = "aby";
  wordlist[2] = "y";
  wordlist[3] = "yousuck";
  wordlist[4] = "z";
  
  int wordnum = 4;
  qsort(wordlist,wordnum,sizeof(char*),compare_string);
  char* test = "aby";
  assert(binary_search(wordlist,test,0,4));
  free(wordlist);
}
void test_search_word_right(void)
{
  FILE* infile=fopen("testgrid.txt","w");
  fprintf(infile, "help\ngaby\nmadd\n");
  fclose(infile);
  char* fileName="testgrid.txt";
  int rows = 0;
  int cols = 0;
  check_grid(fileName, &rows, &cols);
  printf("the things i care about ROW: %d, COL:%d\n", rows, cols);
  char** grid = load_grid(fileName, rows, cols);
  char** wordlist=malloc(15*sizeof(char*));
  wordlist[0] = "help";
  wordlist[1] = "aby";
  wordlist[2] = "y";
  wordlist[3] = "yousuck";
  wordlist[4] = "z";
  wordlist[5]="ple";
  wordlist[6]="bag";
  wordlist[7]="dd";
  wordlist[8]="hgm";
  wordlist[9]="bd";
  wordlist[10]="py";
  wordlist[11]="yp";
  wordlist[12]="ae";
  wordlist[13]="dy";
  wordlist[14]="mgh";
  int wordnum = 15;
  qsort(wordlist,wordnum,sizeof(char*),compare_string);
 /* for (int i=0; i<rows;i++)
  {
    puts(grid[i]);
  }
  for (int i=0; i<5;i++)
  {
    puts(wordlist[i]);
  }

  char* word=malloc(10*sizeof(char));
  for (int i = 0; i < rows ; i++)
  {
    for (int j =0; j < cols; j++)
    {
      search_word_right(grid, wordlist, wordnum,word, i,j,4);
      search_word_left(grid, wordlist, wordnum,word, i,j);
      search_word_down(grid,wordlist,wordnum,word,i,j,3);
      search_word_up(grid,wordlist,wordnum,word,i,j);
    }
  }
*/
  int numrows=3;//hardcode grid size of above testgrid.txt
  int numcols=4;
  search_all_words(grid,wordlist,wordnum,numrows,numcols);
  free(wordlist);
  free(grid);
}

int main(void) {
  test_check_grid();
  test_load_grid();
  test_read_word();
  test_add_to_word_list();
  test_search_word_right();
  test_binary_search();
  printf("All tests passed!!\n");
  return 0;
}
