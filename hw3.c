/*
Gaby Mizrahi, Ariel Goldschlag
600.120
Homework 3
2/27/15
(516) 368-4159
gmizrah1, agoldsc2
gmizrah1@jhu.edu, agoldsc2@jhu.edu
 */
#include "wordsearch.h"

int main(int argc, char* argv[]) {

  if (argc == 1)//checks for valid input
  {
    puts("please input filename in command line");
    return 0;
  }
  char *filename=argv[1];
  int numrows=0;//int to store total number of rows in grid
  int numcols=0;//int to store total number of cols in grid
  
  if(!check_grid(filename,&numrows,&numcols)) return 0;//end program for invalid grids
  
  char ** grid=load_grid(filename,numrows,numcols);
  char* word=NULL;//char* to hold inputted search words
  char** wordlist=malloc(sizeof(char*)*5);//char** to hold list of all search words
  int wordnum=0;//number of words in wordlist
  int listlength=5;//length of wordlist
  printf("Please enter search words, ctrl-d on new line to end input\n"); 
  do    //read in search words from stdin
  {
    word=read_word();
    wordlist = add_to_word_list(wordlist,word,&wordnum, &listlength);
  }while(strcmp(word, "#*"));

  wordnum--;    //last element is junk (#*) freed in method read word
  wordlist=realloc(wordlist,sizeof(char*)*(wordnum));   //realloc to delete empty space
  qsort(wordlist,wordnum,sizeof(char*),compare_string);//sort wordlist alphabetically
  search_all_words(grid, wordlist,wordnum,numrows,numcols);//search all words
 
  //free all malloced memory
  for(int i=0;i<numrows;i++)
  {
    free(grid[i]);
  }
  free(grid);
  for(int i=0;i<wordnum;i++)
  {
    free(wordlist[i]);
  }
  free(wordlist);
  return 1;
}
