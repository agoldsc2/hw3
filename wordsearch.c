/*
Gaby Mizrahi, Ariel Goldschlag
600.120
Homework 3
2/27/15
(516) 368-4159
gmizrah1, agoldsc2
gmizrah1@jhu.edu, agoldsc2@jhu.edu
 */
#include "wordsearch.h"
//method that checks for invalid grids(ie not rectangular grids), returns true if valid and false if invalid. 
//numrows and numcols are the total number of rows and columns in the grid(not the maximum index)
bool check_grid(char* filename, int* numrows, int* numcols)
{
  FILE * infile=fopen(filename, "r");
  int col=0;//current col
  int row=0;//current row
  char hold;
  //reads through entire file
  while((hold=fgetc(infile))!=EOF)
  {
    if(hold == '\n')
    {
      if(col==0)//check for empty line and stop checking grid
      {
        *numrows=row;
        return true;
      }
      if(row !=0 && *numcols!= col) //checking that all rows have equal length ensures that all columns also have equal length
      {
        printf("All rows are not the same length\n");
        return false;
      }
      row++;//increment rows
      *numcols = col;//increment total columns
      col =0;//reset current column
    }
    else
    {
      col++;
    }
  }
  *numrows=row;
  fclose(infile);
  return true;
}//end check_grid

//method creates dynamically sized 2d char array holding grid, read in from file
char** load_grid(char* filename, int numrows, int numcols)
{
 char** grid= malloc(sizeof(char*)*numrows);//create array
    for(int i=0;i<numrows; i++)
    {
      grid[i]=malloc(sizeof(char)*numcols);
    }
 FILE* infile=fopen(filename, "r");
 for(int row=0; row<numrows;row++) //read in from file and save to array
 {
   for(int col=0; col<numcols; col++)
   { 
    grid[row][col]=tolower(fgetc(infile));
   }
   fgetc(infile);
 }
 fclose(infile);
 return grid;
}//end load_grid

//method reads words to search for from std in and returns pointer to each word
//dynamically sizes array to ensure there's enough space to save the word
char* read_word()
{
  char* wordbuffer = malloc(sizeof(char)*5);//create array to save word
  int spot = 0;//spot in word
  int length = 5;// length of wordbuffer
  char temp = 'a';//holds next char in each word until checking that the buffer has enough space for it
  char* ended = "#*";//string to signal end of input words to main
  while ((temp=getchar())!= EOF && !isspace((int)temp))
  {
    if (spot == length -1)//check if buffer is big enough for word, realloc if not 
    {
      wordbuffer = realloc(wordbuffer, 2*length);
      length *=2;//update length 
    }
    wordbuffer[spot] = tolower(temp);//save charachter from buffer
    spot++;
  }
  if(temp==EOF) //stop reading at end of file 
   {
     free(wordbuffer);
     return ended;//returns signal to main that input has ended
    }
  wordbuffer = realloc(wordbuffer,spot+1);//realloc wordbuffer to minimum necessary size
  wordbuffer[spot] = '\0';
  return wordbuffer;
}//end build_word

//method adds every word read in from stdin into wordlist, returns char** of wordlist
//wordnum is the total number of words in the list(not the index of the last word)
//listlength is the size of memory allocated for the array
char** add_to_word_list(char** wordlist, char* word, int* wordnum, int* listlength)
{
  if(*wordnum==*listlength-1)//check if there's enough space in wordlist, realloc if not
  {
     wordlist=realloc(wordlist, sizeof(char*)*(*listlength)*2);
    *listlength = (*listlength)*2;
  }
  wordlist[*wordnum]=word;//add word to wordlist
  *wordnum=*wordnum+1;
  return wordlist;
}//add_to_word list

//method to compare two strings, to be used in qsort
int compare_string(const void* str1,const void* str2)
{
  const char** word1 = (const char**)str1;
  const char** word2 = (const char**)str2;
  return strcmp(*word1, *word2);
}//end compare_string

//method makes word going right from indicated index (row,col) of specified length
void make_word(char** grid,char* word, int row, int col, int length)
{
  for(int i=0; i<length;i++)
    {
      word[i]=grid[row][col+i];
    }
  word[length]='\0';
}//end make_word

//method makes word going down from indicated index (row,col) of specified length
void make_word_down(char** grid,char* word, int row, int col, int length)
{
  for(int i=0; i<length;i++)
  {
    word[i]=grid[row+i][col];
  }
  word[length]='\0';
}//end make_word_down

//reverse a string given the pointer and length
void reverse_string(char* word, int wordlength)
{
  char temp[wordlength + 1];
  for( int i = 0; i < wordlength; i++)
  {
    temp[i] = word[(wordlength-i)-1];
  }
  temp[wordlength] = '\0';
  strcpy(word, temp);
}//end reverse_string

//method conducts binary search for word in wordlist
bool binary_search(char** wordlist, char* word,int startindex, int endindex)
{
  if(startindex>endindex)
  {
   return false;
  }
  int mid=(startindex+endindex)/2;
  if(!strcmp(wordlist[mid],word))//check middle
  {
    return true;
  }
  if(strcmp(wordlist[mid],word)>0) //look left
  {
    return binary_search(wordlist,word,startindex,mid-1);
  }
  else //look right
  {
    return binary_search(wordlist,word,mid+1,endindex);
  }
}//end binary_search


//method to search for every possible word going right, from index (row,col) of grid, in wordlist
void search_word_right(char** grid,char** wordlist,int wordnum,char* word, int row, int col, int numcols){
  bool found=false;
  for (int i =1; i <= numcols - col; i++) //numcols-col is number of words going right from the index
  {
    make_word(grid,word,row,col,i);//makes word of length i
    found=binary_search(wordlist,word,0,wordnum);//searches for word of length i, returns true if found
    if(found)
     {
       printf("%s %d %d R\n", word, row, col);
     }
     
  }
}//end search_word_right

//method to search for every possible word going left, from index (row,col) of grid, in wordlist
void search_word_left(char** grid,char**wordlist,int wordnum,char* word, int row, int col)
{
  bool found=false;
  col++;//add one to get proper lengths of words
  for (int i=col;i>0; i--)
  {
   make_word(grid,word,row,col-i,i);//make all words right from adjusted index
   reverse_string(word,i); //reverses words to find left words
   found=binary_search(wordlist,word,0,wordnum);
    if(found)
     {
       printf("%s %d %d L\n", word, row, col-1);
     }
     
  }
}//end search_word_left

//method to search for every possible word going down, from index (row,col) of grid, in wordlist
void search_word_down(char** grid, char** wordlist, int wordnum, char* word, int row, int col, int numrows)
{
  bool found=false;
  for(int i=1;i<=numrows-row;i++)
  {
    make_word_down(grid,word,row,col,i);
    found=binary_search(wordlist,word,0,wordnum);
    if(found)
    {
      printf("%s %d %d D\n",word,row,col);

    }
  }
}//end search_word_down

//method to search for every possible word going up, from index (row,col) of grid, in wordlist
void search_word_up(char** grid, char** wordlist, int wordnum, char* word, int row, int col)
{
  bool found=false;
  row++;
  for(int i=row;i>0;i--)
  {
    make_word_down(grid,word,row-i,col,i);
    reverse_string(word,i);
    found=binary_search(wordlist,word,0,wordnum);
    if(found)
    {
      printf("%s %d %d U\n",word,row-1,col);
    }
  }
}

//method to search for all possible words of grid in wordlist using search functions
void search_all_words(char** grid,char** wordlist,int wordnum,int numrows, int numcols)
{
  wordnum--;//decrement wordnum because we want the index of the last word, not the number of words
  int maxsize=0;
  if(numrows>numcols) //determines max length of potential word built from grid
  {
    maxsize=numrows;
  }
  else
  {
    maxsize=numcols;
  }
  char* key=malloc((maxsize+1)*sizeof(char));//malloc enough space for any word from grid
  for (int i = 0; i <numrows ; i++)
  {
    for (int j =0; j <numcols; j++)
    {
      search_word_right(grid, wordlist, wordnum,key, i,j,numcols);
      search_word_left(grid, wordlist, wordnum,key, i,j);
      search_word_down(grid,wordlist,wordnum,key,i,j,numrows);
      search_word_up(grid,wordlist,wordnum,key,i,j);
    }
  }
 free(key);
}//end search_all_words

