/*
Gaby Mizrahi, Ariel Goldschlag
600.120
Homework 3
2/27/15
(516) 368-4159
gmizrah1, agoldsc2
gmizrah1@jhu.edu, agoldsc2@jhu.edu
 */
#ifndef _WORDSEARCH_H_
#define _WORDSEARCH_H_ 
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
bool check_grid(char* filename, int* rows, int* cols);

char** load_grid(char* filename, int rows, int cols);

char* read_word();

char** add_to_word_list(char** wordlist, char* word, int* wordnum,int* listlength);

int compare_string(const void*, const void*);

void make_word(char** grid, char* word, int row, int col, int length);

void make_word_down(char** grid, char* word, int row, int col, int length);

void reverse_string(char* word, int wordlength);

bool binary_search(char** wordlist, char* word, int startindex, int endindex);

void search__word_right(char** grid,char** wordlist,int wordnum, char* word, int row, int col, int totalCol);

void search_word_left(char** grid,char**wordlist,int wordnum,char* word, int row, int col);

void search_word_down(char** grid,char**wordlist,int wordnum,char* word, int row, int col, int numRows);

void search_word_up(char** grid,char**wordlist,int wordnum,char* word, int row, int col);

void search_all_words(char** grid,char** wordlist,int wordnum, int numrows, int numcols);

#endif
